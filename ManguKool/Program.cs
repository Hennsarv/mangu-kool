﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MänguKool
{
    static class Program
    {
        public static DateTime ToDatetime(this string isikukood)
        {
            DateTime x;

            DateTime.TryParse(
                (((isikukood[0] - '0' - 1) / 2 + 18).ToString() +
                    isikukood.Substring(1, 2) + "-" +
                    isikukood.Substring(3, 2) + "-" +
                    isikukood.Substring(5, 2))
                    , out x)
                    ;
            return x;
        }
        static IEnumerable<T> MyForEach<T>(this IEnumerable<T> m, Action<T> a)
        {
            foreach (T x in m) a(x);
            return m;
        }

        static List<Inimene> Õpilased()
        {
            return Inimene.Inimesed.Values
                .Where(x=> x.ÕppeKlass != "" && x.ÕppeAine == "")
                .ToList()
                ;
        }

        static List<Inimene> Õpetajad()
        {
            return Inimene.Inimesed.Values
                .Where( x=> x.ÕppeAine != "")
                .ToList()
                ;
        }


        static void Main(string[] args)
        {


                   
            
            string DataPath = @"..\..\DataFiles\";
            string KlassidFailinimi = DataPath + "Klassid.txt";
            string ÕpilasedFailinimi = DataPath + "Õpilased.txt";
            string ÕpetajadFailinimi = DataPath + "Õpetajad.txt";
            string VanemadFailinimi = DataPath + "Vanemad.txt";
            string AinedFailinimi = DataPath + "Ained.txt";

            using (MKDataContext mk = new MKDataContext())
            {
                loll
                mk.DbAines.InsertAllOnSubmit(
                File.ReadAllLines(AinedFailinimi)
                    .Select(x => x.Split(','))
                    .Select(
                    x => new DbAine()
                        {
                            AineKood = x[0],
                            Nimetus = x.Length > 1 ? x[1].Trim() : "",
                            Maht = 0
                        })
                    );
                mk.SubmitChanges();
            }


                File.ReadAllLines(KlassidFailinimi)
                    .Select(x => x.Split(','))
                    .MyForEach(x => Klass.NewKlass(x).Kommentaar = x.Length > 2 ? x[2].Trim() : "")

                    ;
            foreach (var x in Klass.Klassid)
            {
                Console.WriteLine( x );
            }
            Console.WriteLine();

            File.ReadAllLines(AinedFailinimi)
                .Select(x => x.Split(','))
                //.ToList()
                .MyForEach(x => Aine.NewAine(x).Tunde = x.Length > 2 ? int.Parse(x[2]) : 0)
                ;

            File.ReadAllLines(ÕpilasedFailinimi)
                .Select(x => x.Split(','))
                .MyForEach(x => Inimene.NewInimene(x).ÕppeKlass = x.Length > 2 ? x[2].Trim() : "")
                ;

            File.ReadAllLines(ÕpetajadFailinimi)
                .Select(x => x.Split(','))
.MyForEach((Action<string[]>)((string[] x) =>
               {
                   var v = Inimene.NewInimene(x);
                   v.ÕppeAine = x.Length > 2 ? x[2].Trim() : "";
                   v.ÕppeKlass = x.Length > 3 ? x[3].Trim() : "";
                   Klass.GetKlass(v.ÕppeKlass)?.SetKlassijuhataja(x[0]);
               }))
                ;

            File.ReadAllLines(VanemadFailinimi)
                .Select(x => x.Split(','))
                .MyForEach(x =>
                {
                    var v = Inimene.NewInimene(x);
                    if (x.Length > 2) v.LisaLapsed(x[2].Trim());
                })
                ;
                    foreach (var x in Klass.Klassid.Values)
            {
                Console.WriteLine(x + "\n\tÕpilased:");
                foreach (var y in x.Õpilased) Console.WriteLine("\t" + y.ToString());
            }
            foreach (var x in Aine.Ained.Values) Console.WriteLine(x);
            foreach (var x in Inimene.Inimesed.Values)
            {
                Console.WriteLine($"{x} ik {x.Isikukood} vanus ({x.Vanus})");
                if (x.OnVanemaid)
                {
                    Console.WriteLine("\tVanemad:");
                    foreach (var y in x.Vanemad) Console.WriteLine($"\t{y}");
                }
                if (x.OnLapsi)
                {
                    Console.WriteLine("\tLapsed:");
                    foreach (var y in x.Lapsed) Console.WriteLine($"\t{y}");
                }
                if (x.OnÕdesidVendi)
                {
                    Console.WriteLine("\tÕed ja vennad");
                    foreach (var y in x.ÕedVennad) Console.WriteLine($"\t{y}");
                }


            }
            Console.WriteLine(AineHinne.TestHinne);
            Console.WriteLine("{0:n}", AineHinne.TestHinne);
            Console.WriteLine($"{AineHinne.TestHinne: n}");

            Random r = new Random();
            var õpilased = Õpilased();
            var õpetajad = Õpetajad();

            for (int i = 0; i < õpilased.Count; i++)
            {
                for (int j = 0; j < õpetajad.Count; j++)
                {
                    new AineHinne(
                        õpilased[i].Isikukood,
                        õpetajad[j].Isikukood,
                        (Hinne)(r.Next() % 6)
                        );
                }
            }
            
            foreach(var x in AineHinne.Hinded)
                Console.WriteLine(x.ToString("n"));

        }
    }

    

    class Klass 
    {
        public static Dictionary<string, Klass> Klassid
            = new Dictionary<string, Klass>();

        public List<string> KlassiAined = new List<string>();

        public readonly string Kood;
        public readonly string Nimetus;
        public string Kommentaar = "";
        private string _Klassijuhataja = ""; // siia isikukood

        public string Klassijuhataja
        {
            get => _Klassijuhataja; set => _Klassijuhataja = value;
        }

        public IEnumerable<Inimene> Õpilased
        {
            get => Inimene.Inimesed.Values.Where((Func<Inimene, bool>)(x => (bool)(x.ÕppeKlass == this.Kood && x.ÕppeAine == "")));
        }

        public void SetKlassijuhataja(string klassijuhataja)
        {
            _Klassijuhataja = klassijuhataja;
        }


        private Klass(string kood, string nimetus)
        {
            Kood = kood;
            Nimetus = nimetus;
            Klassid.Add(kood, this);
        }

        public static Klass NewKlass(string kood, string nimetus)
        {
            return Klassid.ContainsKey(kood)
                ? Klassid[kood]
                : (new Klass(kood, nimetus));
        }

        public static Klass NewKlass(string[] p)
        {
            return NewKlass(p[0].Trim(), p.Length > 1 ? p[1].Trim() : "");
        }

        public static Klass GetKlass(string kood)
        {
            //return Klassid.ContainsKey(kood) ? Klassid[kood] : null;
            Klassid.TryGetValue(kood, out Klass x);
            return x;
        }


        public override string ToString()
        {
            return $"Klass: {Kood} - {Nimetus} " +
                $" {(Kommentaar == "" ? "" : "(" + Kommentaar + ")")}" +
                $" {(Klassijuhataja == "" ? "" : "(klassijuhataja: " + Inimene.GetInimene(this.Klassijuhataja).ToString() + ")")}";
        }
        
    }

    class Aine
    {
        static int mitu = 0;
        int mitmes = mitu++;

        public static Dictionary<string, Aine> Ained
            = new Dictionary<string, Aine>();

        public readonly string Kood;
        public string Nimetus;
        public int Tunde = 0;

        private Aine(string kood, string nimetus)
        {
            Kood = kood;
            Nimetus = nimetus;
            Ained.Add(kood, this);
        }

        public static Aine NewAine(string kood, string nimetus)
        {
            return (Ained.ContainsKey(kood))
                ? Ained[kood]
                : (new Aine(kood, nimetus));
        }

        public static Aine NewAine(string [] p)
        {
            return NewAine(p[0].Trim(), p.Length > 1 ? p[1].Trim() : "");
        }

        public static Aine GetAine(string kood)
        {
            return Ained.ContainsKey(kood) ? Ained[kood] : null;
        }


        public override string ToString()
        {
            return $"Õppeaine: {Kood} - {Nimetus} {(Tunde == 0 ? "" : $"(maht {Tunde} tundi)" )}";
        }


    }



    class Inimene
    {
        public static Dictionary<string, Inimene> Inimesed
            = new Dictionary<string, Inimene>();


        string _Isikukood = "";
        public string _Nimi = "";

        public string Isikukood { get => _Isikukood; }
        public string Nimi { get => _Nimi; }
        List<string> _Vanemad = new List<string>(); // siia vanemate isikukoodid
        List<string> _Lapsed = new List<string>(); // siia laste isikukoodid   

        string _Klass = ""; // siia klassi kood (õpetajal, kui on klassijuhataja)
        string _Aine = "";  // siia aine kood
        

        public IEnumerable<Inimene> Vanemad
        {
            get => _Vanemad.Select(x => GetInimene(x)).Where(x => x != null);
        }

        public bool OnLapsi
        {
            get { return _Lapsed.Count > 0; }
        }

        public bool KasOnLapsi()
        {
            return _Lapsed.Count > 0;
        }

        public bool OnVanemaid
        {
            get => _Vanemad.Count > 0;
        }


        public IEnumerable<Inimene> Õpilased
        {

            get =>
                (this._Aine == "" ? new List<Inimene>() : 
                Inimene.Inimesed.Values
                .Where(x => x._Klass != "" && x._Aine == "")
                .Where(x => Klass.Klassid[_Klass].KlassiAined.Contains(_Aine)))
                
                ;
        }

        public IEnumerable<Inimene> ÕedVennad
        {
            get => 
                this.OnVanemaid ?
                Inimene.Inimesed.Values
                .Where( x => x != this)
                .Where( x => x.Vanemad
                            .Where(y => this.Vanemad.Contains(y)).Count() >0)
                
                : new List<Inimene>()
                ;
        }

        public bool OnÕdesidVendi
        {
            get => this.OnVanemaid && ÕedVennad.Count() > 0;
        }

        public int Vanus
        {
            get => (int)((DateTime.Now - _Isikukood.ToDatetime()).TotalDays / 365.25);
            
        }

        public IEnumerable<Inimene> Lapsed
        {
            get => _Lapsed.Select(x => GetInimene(x)).Where(x => x != null);
        }

        public string ÕppeKlass
        { get => _Klass; set => _Klass = value; }

        public string ÕppeAine
        {
            get => _Aine; set => _Aine = value;
        }

        private Inimene(string isikukood, string nimi)
        {
            _Isikukood = isikukood;
            _Nimi = nimi;
            Inimesed.Add(isikukood, this);
        }

        public static Inimene NewInimene(string isikukood, string nimi)
        {
            // siia võiks tulla isikukoodi korrektsuse kontroll
            return 
                Inimesed.ContainsKey(isikukood) 
                ? Inimesed[isikukood] 
                : (new Inimene(isikukood, nimi));
        }

        public Inimene LisaLapsed(string lapsed)
        {
            lapsed.Split(' ').ToList()
                .ForEach(x =>
                        {
                            _Lapsed.Add(x);
                            Inimene.GetInimene(x)?.LisaVanem(this._Isikukood);
                        }
                        );

            return this;
        }

        public Inimene LisaVanem(string vanem)
        {
            this._Vanemad.Add(vanem);
            return this;
        }

        public static Inimene NewInimene(string[] p)
        {
            return NewInimene(p[0], p.Length > 1 ? p[1] : "");
        }

        public static Inimene GetInimene(string isikukood)
        {
            return (isikukood != null && Inimesed.ContainsKey(isikukood)) ? Inimesed[isikukood] : null;
        }

        public override string ToString()
        {
            return
              (this.ÕppeAine != "" ? Aine.GetAine(this.ÕppeAine).Nimetus + " õpetaja " :
              this.ÕppeKlass != "" ? Klass.GetKlass(this.ÕppeKlass).Nimetus + " õpilane "
              : "Lapsevanem ") + this._Nimi;

        }

    }

    public enum Hinne { puudub, puudulik, rahuldav, hea, vägahea, suurepärane}


    public class AineHinne : IFormattable
    {
        public static List<AineHinne> Hinded = new List<AineHinne>();
        string isikukood = "";
        string ainekood = "";
        string õpetajakood = ""; // isikukood
        DateTime hindamisaeg = DateTime.Now;
        Hinne hinne = Hinne.puudub;

        private AineHinne() { }

        public AineHinne(string kellele, string kespani, Hinne hinne)
        {
            this.isikukood = kellele;
            this.õpetajakood = kespani;
            this.hinne = hinne;
            this.ainekood = Inimene.Inimesed[kespani].ÕppeAine;
            this.hindamisaeg = DateTime.Now;
            Hinded.Add(this);
        }


        public static AineHinne TestHinne
        {
            get
            {
                AineHinne ah = new AineHinne();
                ah.isikukood = "50101011234";
                ah.ainekood = "mata";
                ah.hinne = Hinne.suurepärane;
                return ah;
            }
        }

        public override string ToString()
        {
            return this.ToString("f");
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return this.ToString(format);
        }

        public string ToString(string format )
        {
            switch (format)
            {
                case "f":
                    return $"Hinne: I:{isikukood} A:{ainekood} Õ:{õpetajakood} D:{hindamisaeg} H:{hinne} ";

                case "s":
                    return $"Hinne {(int)hinne} ({hinne})";

                case "n":
                    return $"Õpilase { Inimene.GetInimene(isikukood)?._Nimi}  hinne aines {Aine.GetAine(ainekood).Nimetus}: {(int)hinne} ({hinne}) ";
                default:
                    return this.ToString();

            };


        }
    }
}
