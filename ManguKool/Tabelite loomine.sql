﻿create table Inimene
(
	Isikukood char(11) primary key,
	Nimi nvarchar(30) not null,
	Klass char(5) null,
	Aine char(5) null
)

create table Aine
(
	AineKood char(5) primary key,
	Nimetus nvarchar(30) not null,
	Maht int null  -- ainemaht tundides
)

create table KelleLapsed
(
	jrknr int identity primary key,
	VanemaIsikukood char(11),
	LapseIsikukood char(11)
	--,	unique (Vanemaisikukood, Lapseisikukood)
)

create table Klass
(
	KlassiKood char(5) primary key,
	Nimetus nvarchar(30)
)

create table Hinne
(
	jrknr int identity primary key,
	OpilaneIK char(11) not null,
	OpetajaIK char(11) null,
	Aine char(5) not null,
	Kuupäev datetime null,
	Hinne int
)